import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-app.js";
import { getDatabase, onValue, ref, set, child, get, update, remove} from "https://www.gstatic.com/firebasejs/9.17.2/firebase-database.js";
import { getStorage,ref as refStorage, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyDVSBxqzIDwch1-0SmA7emDj6RfCACDyU8",
    authDomain: "proyectofinal-80d6b.firebaseapp.com",
    databaseURL: "https://proyectofinal-80d6b-default-rtdb.firebaseio.com",
    projectId: "proyectofinal-80d6b",
    storageBucket: "proyectofinal-80d6b.appspot.com",
    messagingSenderId: "226801522491",
    appId: "1:226801522491:web:af16b881af59939df2c91e"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

let productos = document.getElementById('contenido-productos');
window.addEventListener('DOMContentLoaded',mostrarProductos);
let deportivo = document.getElementById('deportivos');
let deportivoC = document.getElementById('deportivosClasicos');
let cupe = document.getElementById('cupes');
let todoT = document.getElementById('todoterrenos');
let turismo = document.getElementById('turismos');
let sedan = document.getElementById('sedanes');
let muscleC = document.getElementById('muscleCars');
let supers = document.getElementById('supers');
let monoV = document.getElementById('monoVolumenes');

function mostrarProductos(){
    const dbRef = ref(db, "productos");


    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
        
        if(childData.estatus=="1"){

            productos.innerHTML +=`
                <div class='producto'>
                <img class='img-item' src='${childData.urlImagen}'>
                <p class='nombre'>${childData.nombre}</p>
                <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                <p class='precio'>\$${childData.precio}</p>
                <button class='boton-comprar' data-bs-toggle="modal" data-bs-target="#exampleModalToggle">Comprar</button>
                </div>
            `;
        }
        
        });
    },
    {
        onlyOnce: true,
    }
    );
}




function mostrarDeportivos(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Deportivos";
    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==1 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
} 


function mostrarDeportivosC(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Deportivos clásicos";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
            snapshot.forEach((childSnapshot) => {
                const childData = childSnapshot.val();
                
                
                if(childData.categoria==2 && childData.estatus=="1"){
                    productos.innerHTML +=`
                        <div class='producto'>
                        <img class='img-item' src='${childData.urlImagen}'>
                        <p class='nombre'>${childData.nombre}</p>
                        <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                        <p class='precio'>\$${childData.precio}</p>
                        <button class='boton-comprar'>Comprar</button>
                        </div>
                    `;
                    
                }
                
                
            });
    },
    
    {
        onlyOnce: true,
    }
    );
    
} 

function mostrarCupes(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Cupés";
    
    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            

                if(childData.categoria==3 && childData.estatus=="1"){
                    productos.innerHTML +=`
                        <div class='producto'>
                        <img class='img-item' src='${childData.urlImagen}'>
                        <p class='nombre'>${childData.nombre}</p>
                        <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                        <p class='precio'>\$${childData.precio}</p>
                        <button class='boton-comprar'>Comprar</button>
                        </div>
                    `;
                    
                }
                
                
            });
        },
        
        {
            onlyOnce: true,
        }
        );
        
    } 

function mostrarTodoT(){
    const dbRef = ref(db, "productos");

    document.getElementById('tituloProductos').innerHTML = "Todo-terrenos";
    
    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==4 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
}
);

} 

function mostrarTurismos(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Turismos";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==5 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
}

function mostrarSedanes(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Sedanes";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==6 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
} 

function mostrarMuscle(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Muscle Cars";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==7 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
} 

function mostrarSupers(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Supers";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==8 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
} 

function mostrarMono(){
    const dbRef = ref(db, "productos");
    
    document.getElementById('tituloProductos').innerHTML = "Mono-volúmenes";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childData = childSnapshot.val();
            
            
            if(childData.categoria==9 && childData.estatus=="1"){
                productos.innerHTML +=`
                    <div class='producto'>
                    <img class='img-item' src='${childData.urlImagen}'>
                    <p class='nombre'>${childData.nombre}</p>
                    <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                    <p class='precio'>\$${childData.precio}</p>
                    <button class='boton-comprar'>Comprar</button>
                    </div>
                `;
                
            }
            
            
        });
    },
    
    {
        onlyOnce: true,
    }
    );
    
} 

function mostrarTodos(){
    const dbRef = ref(db, "productos");

    document.getElementById('tituloProductos').innerHTML = "Nuestros Productos";

    onValue(dbRef,(snapshot) => {
        productos.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
        
        if(childData.estatus=="1"){
            productos.innerHTML +=`
                <div class='producto'>
                <img class='img-item' src='${childData.urlImagen}'>
                <p class='nombre'>${childData.nombre}</p>
                <p class='cantidad'>Cantidad: ${childData.cantidad}</p>
                <p class='precio'>\$${childData.precio}</p>
                <button class='boton-comprar'>Comprar</button>
                </div>
            `;
        }
        
        });
    },
    {
        onlyOnce: true,
    }
    );
}

deportivo.addEventListener('click', mostrarDeportivos);
deportivoC.addEventListener('click', mostrarDeportivosC);
cupe.addEventListener('click', mostrarCupes);
todoT.addEventListener('click', mostrarTodoT);
turismo.addEventListener('click', mostrarTurismos);
sedan.addEventListener('click', mostrarSedanes);
muscleC.addEventListener('click', mostrarMuscle);
supers.addEventListener('click', mostrarSupers);
monoV.addEventListener('click', mostrarMono);
todos.addEventListener('click', mostrarTodos);

