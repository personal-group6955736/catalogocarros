import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, signOut, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.17.2/firebase-auth.js";
  
const firebaseConfig = {
    apiKey: "AIzaSyDVSBxqzIDwch1-0SmA7emDj6RfCACDyU8",
  authDomain: "proyectofinal-80d6b.firebaseapp.com",
  databaseURL: "https://proyectofinal-80d6b-default-rtdb.firebaseio.com",
  projectId: "proyectofinal-80d6b",
  storageBucket: "proyectofinal-80d6b.appspot.com",
  messagingSenderId: "226801522491",
  appId: "1:226801522491:web:af16b881af59939df2c91e"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);




function login(){
    event.preventDefault();
    let email = document.getElementById('email').value;
    let contra = document.getElementById('contraseña').value;

    if(email == "" || contra == ""){
        alert('Complete los campos');
        return;
    }
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, contra).then((userCredential) => {
    alert('Bienvenido ' + email);
    sessionStorage.setItem('isAuth',"true");
    window.location.href = 'administrador.html';
    
    })
    .catch((error) => {
        alert('Usuario y o contraseña incorrectos')
    });

}



var btnCerrarSesion = document.getElementById('btnCerrarSesion');

if(btnCerrarSesion){
    btnCerrarSesion.addEventListener('click',  (e)=>{
        signOut(auth).then(() => {
        alert("SESIÓN CERRADA")
        window.location.href="index.html";
        }).catch((error) => {
        });
    });
}

onAuthStateChanged(auth, async user => {
    console.log(window.location.pathname);
    if (user) {
    
    } else {
        if (window.location.pathname.includes("administrador")) {
            window.location.href = "/index.html";
        }
    }
});

var botonLogin = document.getElementById('btnIniciarSesion');

if(botonLogin){
    botonLogin.addEventListener('click', login);
}